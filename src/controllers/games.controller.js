'use strict';
const Games = require('../models/games.models');

exports.findAll = function(req, res) {
    Games.findAll(req.query.id_user, function(err, game) {
        console.log('controller')
        if (err) res.send(err);
        console.log('res', game);
        res.header("Access-Control-Allow-Origin", "*");
        res.status(200).json({ data: game });
    });
};

exports.create = function(req, res) {
    const new_game = new Games(req.body);
    //handles null error
    if (!(req.body.score.toString() && req.body.date && req.body.number_question && req.body.id_user && req.body.id_category)) {
        res.status(400).send({ error: true, message: 'Please provide all required field' });
    } else {
        Games.create(new_game, function(err, game) {
            if (err) res.send(err);
            res.json({ error: false, message: "Game added successfully!", data: game });
        });
    }
};
