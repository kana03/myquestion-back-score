const express = require('express')
const router = express.Router()
const gamesController = require('../controllers/games.controller');

// Retrieve all Games
router.get('/getScore', gamesController.findAll);

// Create a new game
router.post('/saveScore', gamesController.create);

module.exports = router