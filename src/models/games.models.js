'use strict';
const { dbConn } = require('../config/db.config');

//Game object create
var Games = function(game) {
    this.score = game.score;
    this.date = new Date();
    this.number_question = game.number_question;
    this.id_user = game.id_user;
    this.id_category = game.id_category;
};
Games.create = function(new_game, result) {

    dbConn.query("INSERT INTO games set ?", new_game, function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
};
Games.findAll = function(id, result) {
    dbConn.query("SELECT score, date, id_category, number_question FROM games WHERE id_user = ? ORDER BY date ASC", [id], function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            console.log('games : ', res);
            result(null, res);
        }
    });
};
module.exports = Games;
